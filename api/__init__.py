import os

from flask import Flask

from api.constants import APP_CONFIG_ENV_VAR, DEV_CONFIG_VAR, PROD_CONFIG_VAR, APP_NAME


def config_app(app_instance):
    """
    Sets the app_instance configurations in the order of:
        - Check ENV VAR
        - Load configuration object
        - Load configuration file
    The order of precedence is thus bottom-up

    Args:
        - app_instance: An instance of Flask App
    """

    config_type = get_config_type()

    # Possible configurations as a dictionary
    configs = {
        DEV_CONFIG_VAR: "api.config.DevelopmentConfig",
        PROD_CONFIG_VAR: "api.config.ProductionConfig"
    }

    app_instance.config.from_object(configs[config_type])

    config_file_path = os.environ.get(APP_NAME + "_APP_CONFIG_FILE", "")

    if config_file_path and os.path.exists(config_file_path):
        app_instance.config.from_pyfile(config_file_path)

    # Ensure flask doesn't redirect to trailing slash endpoint
    app_instance.url_map.strict_slashes = False


def get_config_type():
    return os.environ.get(APP_CONFIG_ENV_VAR, DEV_CONFIG_VAR).lower().strip()


app = Flask(__name__)

config_app(app)


@app.route("/")
def index():
    return "kiondooleo-api"

