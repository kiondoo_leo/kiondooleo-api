from setuptools import setup, find_packages

from api.constants import APP_NAME

setup(
    name=APP_NAME,
    version="1.0.0",
    description="KiondooLeo App Backend API",
    long_description=open("README.md").read(),
    packages=find_packages(),
    include_package_data=True,
    install_requires=[],
    author="Eddy Mwenda, Jeff Ndungu, Lorna Macharia, Timothy Mutagi",
    author_email="mwendaeddy@gmail.com, jeffkim207@gmail.com, lornamach@gmail.com", "mutegi.timothy538@gmail.com",
)
