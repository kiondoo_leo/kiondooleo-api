Backend API for the KiondooLeo App

## Built with
- Python Flask

## Authors
- Eddy Mwenda (mwendaeddy@gmail.com)
- Geoffrey Ndungu (jeffkim207@gmail.com)
- Timothy Mutegi (mutegi.timothy538@gmail.com)
- Lorna Macharia (lornamach@gmail.com)

## License
...
